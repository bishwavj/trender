"use strict";

const path = require("path");
const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const errorhandler = require("errorhandler");
const fileUpload = require("express-fileupload");
const bodyParser = require("body-parser");
const redis = require("redis");
const session = require("express-session");
const { v4: uuidv4 } = require("uuid");
var fs = require("fs");

var config = require("./src/config/config");

var app = express();

var router = express.Router();

app.use(helmet());

app.set("trust proxy", 1);
app.set("httpsPort", config.httpsPort);
app.set("views", __dirname + "/src/views");
app.set("view engine", "ejs");
app.use("/static", express.static(path.join(__dirname, "public")));

var RedisStore = require("connect-redis")(session);

var client = redis.createClient({
  host: config.session_redis.host,
  port: config.session_redis.port,
  // password: config.session_redis.password,
  db: config.session_redis.db,
});
client.on("error", (err) => {
  console.log("Redis connection error!", err);
  process.exit(1);
});
client.on("ready", () => {
  console.log("Redis connected.");
});

app.use(
  session({
    key: config.session.key,
    store: new RedisStore({ client }),
    secret: config.session.secret,
    saveUninitialized: false,
    resave: false,
    cookie: {
      domain: config.cookie.domain,
      httpOnly: config.cookie.httpOnly,
      maxAge: config.cookie.maxAge,
    },
  })
);

morgan.token("id", function getId(req) {
  return req.id;
});

app.use(assignId);
app.use(
  morgan(
    "[:date[iso] #:id] \x1b[36mStarted\x1b[0m     :method  :remote-addr  :url",
    { immediate: true }
  )
);
app.use(
  morgan(
    "[:date[iso] #:id] \x1b[33mCompleted\x1b[0m   :status  :remote-addr  :url  :res[content-length] in :response-time ms"
  )
);

if (app.get("env") === "development") {
  app.use(errorhandler());
}

const adminAuth = require("./src/routes/trender/admin");
const userAuth = require("./src/routes/trender/user");
const categoryRoute = require("./src/routes/trender/category");
const productRoute = require("./src/routes/trender/product");

app.use(bodyParser.urlencoded({ extended: true, limit: "30mb" }));
app.use(bodyParser.json());

app.use("/api", adminAuth);
app.use("/api", userAuth);
app.use("/api", categoryRoute);
app.use("/api", productRoute);

function assignId(req, res, next) {
  req.id = uuidv4();
  next();
}

module.exports = app;
