"use strict";
const jwt = require("jsonwebtoken");
const express = require("express");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");
// const multerS3 = require("multer-s3");
// const aws = require("aws-sdk");

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, path.join(path.dirname(__dirname), "uploads"));
//   },
//   filename: function (req, file, cb) {
//     cb(null, shortid.generate() + "-" + file.originalname);
//   },
// });

// const accessKeyId = process.env.accessKeyId;
// const secretAccessKey = process.env.secretAccessKey;

// const s3 = new aws.S3({
//   accessKeyId,
//   secretAccessKey,
// });
exports.fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "../uploads");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

exports.upload = multer({ storage: this.fileStorage });

// exports.uploadS3 = multer({
//   storage: multerS3({
//     s3: s3,
//     bucket: "flipkart-clone-app",
//     acl: "public-read",
//     metadata: function (req, file, cb) {
//       cb(null, { fieldName: file.fieldname });
//     },
//     key: function (req, file, cb) {
//       cb(null, shortid.generate() + "-" + file.originalname);
//     },
//   }),
// });

var ec = require("../../src/lib/error_consts");
var config = require("../../src/config/config");

exports.validateToken = (req, res, next) => {
  if (!req.headers.authorization) {
    return next(
      ec.appError({
        status: ec.UNAUTHORIZED_ACCESS,
        error: "authorization required",
      })
    );
  }
  const token = req.headers.authorization.split(" ")[1];
  jwt.verify(token, process.env.JWT_SECRET, function (err, user) {
    if (!user) {
      return next(
        ec.appError({
          status: ec.UNAUTHORIZED_ACCESS,
          error: "Invalid token",
        })
      );
    }

    req.user = user;

    return next();
  });
};

exports.requireSignin = (req, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(" ")[1];
    //     const user = jwt.verify(token, process.env.JWT_SECRET);
    const data = auth.validateToken(token);
    console.log(data);
    req.user = user;
  } else {
    return res.status(400).json({ error: "Authorization required" });
  }
  next();
  //jwt.decode()
};

exports.userMiddleware = (req, res, next) => {
  if (req.user.role !== "user") {
    return res.status(400).json({ error: "User access denied" });
  }
  next();
};

exports.adminMiddleware = (req, res, next) => {
  //   console.log(req.user);
  if (req.user.role !== "admin") {
    //     if (req.user.role !== "super-admin") {
    return res.status(400).json({ error: "Admin access denied" });
    //     }
  }
  next();
};

exports.superAdminMiddleware = (req, res, next) => {
  if (req.user.role !== "super-admin") {
    return res.status(200).json({ message: "Super Admin access denied" });
  }
  next();
};
