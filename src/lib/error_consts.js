"use strict";
var define = require("node-constants")(exports);

define("LOGIN_FAILURE", 901);
define("EMPTY_USER_PASS", 902);
define("UNAUTHORIZED_ACCESS", 903);
define("DB_ERROR", 904);
define("UNKNOWN_ERROR", 905);
define("INETRNAL_ERROR", 906);
define("INVALID_PARAM", 907);
define("INVALID_DATA", 908);
define("UNDEFINED_DATA", 909);
define("NOT_FOUND", 910);
define("USER_EXISTS", 911);

var appError = function (obj) {
  if (obj) {
    var err_msg = obj.error || "Internal Error";
    var err = new Error(err_msg);
    err.status = obj.status;
    return err;
  }
};

var checkError = function (err) {
  if (err) {
    console.log("DB Error:", err);
  }
};

define("appError", appError);
define("checkError", checkError);
