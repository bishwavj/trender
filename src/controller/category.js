const Category = require("../model/category");
const Product = require("../model/product");
const slugify = require("slugify");
const shortid = require("shortid");

function createCategories(categories, parentId = null) {
  const categoryList = [];
  let category;
  if (parentId == null) {
    category = categories.filter((cat) => cat.parentId == undefined);
  } else {
    category = categories.filter((cat) => cat.parentId == parentId);
  }

  for (let cate of category) {
    categoryList.push({
      _id: cate._id,
      name: cate.name,
      description: cate.description,
      availability: cate.availability,
      slug: cate.slug,
      parentId: cate.parentId,
      categoryImage: cate.categoryImage,
      children: createCategories(categories, cate._id),
    });
  }
  return categoryList;
}

exports.addCategory = async (req, res) => {
  if (!req.body.name)
    return res.status(400).json({ error: "name is required" });
  const name = req.body.name;
  const data = await Category.findOne({ name }).exec();
  if (data) return res.status(400).json({ error: "name already in use" });

  categoryObj = {
    name: req.body.name,
    description: req.body.description,
    slug: `${slugify(req.body.name)}`,
    categoryImage: req.body.file,
    createdBy: req.user._id,
  };

  if (req.file) {
    categoryObj.categoryImage = "/public/" + req.file.filename;
  }

  if (req.body.parentId) {
    const id = req.body.parentId;
    const validId = await Category.findOne({ id }).exec();
    if (validId) {
      categoryObj.parentId = req.body.parentId;
    } else return res.status(400).json({ error: "invalid parentId" });
  }

  const cat = new Category(categoryObj);

  cat.save((error, category) => {
    if (error) return res.status(400).json({ error });
    if (category) {
      return res.status(201).json({ category });
    }
  });
};

exports.uploads = (req, res) => {
  console.log(req.body);
  res.status(200).json({ file: req.file, body: req.body });
};

exports.getCategories = (req, res) => {
  Category.find({}).exec((error, categories) => {
    if (error) return res.status(400).json({ error });
    if (categories) {
      const categoryList = createCategories(categories);
      res.status(200).json({ categoryList });
    }
  });
};

exports.getCategory = (req, res) => {
  const { id } = req.params;
  Category.findById(id).exec((error, category) => {
    if (error) return res.status(400).json({ error });
    else if (category) {
      res.status(200).json({ category });
    } else res.status(400).json({ error: "not found" });
  });
};

exports.updateCategory = async (req, res) => {
  const _id = req.params.id;
  const { name, parentId, description, availability } = req.body;
  const image = req.file;
  var data = await Category.findOne({ _id }, (err, value) => {
    if (err) return res.status(400).json({ error: "invalid id" });
    if (value) data = value;
  }).exec();
  categoryObj = {
    name: name ? name : data.name,
    description: description ? description : data.description,
    availability: availability ? availability : data.availability,
    slug: name ? `${slugify(name)}` : data.slug,
    parentId: parentId ? parentId : data.parentId || null,
  };

  if (req.file) {
    categoryObj.categoryImage = "/public/" + req.file.filename;
  }

  Category.findOneAndUpdate(
    { _id },
    categoryObj,
    {
      new: true,
    },
    (err, value) => {
      if (err) return res.status(400).json({ error: "operation failed" });
      else return res.status(201).json({ updatedCategory: value });
    }
  );
};

exports.deleteCategory = async (req, res) => {
  const { id } = req.params;
  const deletedCategory = await Category.findOneAndDelete({
    _id: id,
  });
  if (deletedCategory) {
    res.status(201).json({ message: "category removed" });
  } else {
    res.status(400).json({ error: "unable to delete" });
  }
};
