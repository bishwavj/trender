const Product = require("../model/product");
const shortid = require("shortid");
const slugify = require("slugify");
const Category = require("../model/category");

exports.createProduct = async (req, res) => {
  if (!req.body.name || !req.body.category)
    return res.status(400).json({ error: "name & category are required" });
  const name = req.body.name;
  const data = await Product.findOne({ name }).exec();
  if (data) return res.status(400).json({ error: "name already in use" });

  const {
    price,
    description,
    quantity,
    sku,
    weight,
    brand,
    type,
    color,
    variant,
    seotitle,
    seodesc,
    seokeyword,
    seoheader,
  } = req.body;
  let productPictures = [];

  if (req.files && req.files.length > 0) {
    productPictures = req.files.map((file) => {
      return { img: file.location };
    });
  }

  const product = new Product({
    name: name,
    slug: slugify(name),
    price,
    quantity,
    description,
    sku,
    weight,
    createdBy: req.user._id,
    productPictures,
    attributes: {
      brand,
      type,
      color,
      variant,
    },
    seo: {
      title: seotitle,
      description: seodesc,
      keyword: seokeyword,
      slug: seotitle ? slugify(seotitle) : null,
      header: seoheader,
    },
  });

  const id = req.body.category;
  const validId = await Category.findOne({ _id: id }).exec();
  if (validId) {
    product.category = req.body.category;
  } else return res.status(400).json({ error: "invalid category" });

  product.save((error, product) => {
    if (error) return res.status(400).json({ error });
    else if (product) {
      res.status(201).json({ product, files: req.files });
    }
  });
};

exports.updateProduct = async (req, res) => {
  if (req.body.length < 0)
    return res.status(400).json({ error: "update data required" });
  const _id = req.params.id;

  const {
    name,
    price,
    description,
    quantity,
    sku,
    weight,
    brand,
    type,
    color,
    variant,
    seotitle,
    seodesc,
    seokeyword,
    seoheader,
  } = req.body;

  const image = req.file;
  let data;
  Product.findOne({ _id }).exec((error, value) => {
    if (error || !value) return res.status(400).json({ error: "invalid id" });
    else if (value) data = value;
  });

  let productPictures = [];
  if (req.files && req.files.length > 0) {
    productPictures = req.files.map((file) => {
      return { img: file.location };
    });
  }

  productObj = {
    name: name ? name : data.name,
    slug: name ? slugify(name) : data.slug,
    price: price ? price : data.price,
    quantity: quantity ? quantity : data.quantity,
    description: description ? description : data.description,
    sku: sku ? sku : data.sku,
    weight: weight ? weight : data.weight,
    productPictures: productPictures
      ? productPictures
      : data.productPictures || null,
    attributes: {
      brand: brand ? brand : null,
      type: type ? type : null,
      color: color ? color : null,
      variant: variant ? variant : null,
    },
    seo: {
      title: seotitle ? seotitle : null,
      description: seodesc ? seodesc : null,
      keyword: seokeyword ? seokeyword : null,
      slug: seotitle ? slugify(seotitle) : null,
      header: seoheader ? seoheader : null,
    },
  };

  if (req.file) {
    productObj.productImage = "/public/" + req.file.filename;
  }

  Product.findOneAndUpdate(
    { _id },
    productObj,
    { upsert: true, new: true },
    (err, value) => {
      if (err) return res.status(400).json({ error: "operation failed" });
      else return res.status(201).json({ updatedCategory: value });
    }
  );
};

exports.addfilestoProduct = async (req, res) => {
  const { id } = req.params;
  const { name, description } = req.body;
  const update = {
    fileAddon: { name, description },
  };
  if (req.file) {
    update.fileAddon.file = "/public/" + req.file.filename;
  }
  Product.findOneAndUpdate({ _id: id }, update, {
    upsert: true,
    new: true,
  }).exec((err, data) => {
    if (err) res.status(400).json({ err });
    else return res.status(201).json(data);
  });
};

exports.deleteProductById = (req, res) => {
  const { id } = req.params;
  Product.findOneAndDelete(id, (err, data) => {
    if (err) res.status(400).json({ error: "unable to delete" });
    else if (data) res.status(201).json({ message: "product removed" });
    else res.status(400).json({ error: "product not found" });
  });
};

exports.getProducts = (req, res) => {
  Product.find({}).exec((err, data) => {
    if (err) res.status(400).json({ error });
    else return res.status(400).json(data);
  });
};
