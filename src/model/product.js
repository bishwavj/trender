const mongoose = require("mongoose");
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      required: false,
      unique: true,
    },
    sku: { type: String },
    weight: { type: String },
    description: {
      type: String,
      required: true,
      trim: true,
    },
    availability: {
      type: Boolean,
      default: true,
    },
    price: {
      type: Number,
      default: undefined,
    },
    quantity: {
      type: Number,
      required: true,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    productPictures: [{ img: { type: String } }],

    attributes: {
      brand: { type: String },
      type: { type: String },
      color: { type: String },
      variant: { type: String },
    },
    fileAddon: {
      name: { type: String },
      description: { type: String },
      file: { type: String },
    },
    seo: {
      title: { type: String },
      description: { type: String },
      keyword: { type: String },
      slug: { type: String },
      header: { type: String },
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Product", productSchema);
