"use strict";
const express = require("express");
const router = express.Router();
var error = require("../error");

const { signup, signin } = require("../../controller/userAuth");
const {
  validateSignupRequest,
  isRequestValidated,
  validateSigninRequest,
} = require("../../lib/validator");

router
  .post(
    "/user/signup",
    validateSignupRequest,
    isRequestValidated,
    signup,
    error
  )
  .post(
    "/user/signin",
    validateSigninRequest,
    isRequestValidated,
    signin,
    error
  );

module.exports = router;
