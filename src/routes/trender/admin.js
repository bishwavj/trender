"use strict";
const express = require("express");
const router = express.Router();
var error = require("../error");

const { signup, signin, signout } = require("../../controller/adminAuth");

const {
  validateSignupRequest,
  isRequestValidated,
  validateSigninRequest,
} = require("../../lib/validator");

router
  .post(
    "/admin/signup",
    validateSignupRequest,
    isRequestValidated,
    signup,
    error
  )
  .post(
    "/admin/signin",
    validateSigninRequest,
    isRequestValidated,
    signin,
    error
  )
  .post("/admin/signout", signout, error);

module.exports = router;
