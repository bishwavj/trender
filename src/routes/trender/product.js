const express = require("express");
//const {  } = require('../controller/category');
const {
  validateToken,
  requireSignin,
  adminMiddleware,
} = require("../../lib/middleware");
const {
  createProduct,
  addfilestoProduct,
  getProductsBySlug,
  updateProduct,
  deleteProductById,
  getProducts,
} = require("../../controller/product");
const multer = require("multer");
const router = express.Router();
const shortid = require("shortid");
const path = require("path");
var error = require("../error");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "../../uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage });

router.post(
  "/product",
  validateToken,
  adminMiddleware,
  upload.array("image"),
  createProduct,
  error
);

router.put(
  "/product/:id",
  validateToken,
  adminMiddleware,
  upload.array("image"),
  updateProduct,
  error
);

router.post(
  "/product/:id",
  validateToken,
  adminMiddleware,
  upload.single("file"),
  addfilestoProduct,
  error
);

router.delete(
  "/product/:id",
  validateToken,
  adminMiddleware,
  deleteProductById
);
router.get("/product", getProducts);

module.exports = router;
