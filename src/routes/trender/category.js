const express = require("express");
const {
  addCategory,
  getCategory,
  getCategories,
  updateCategory,
  deleteCategory,
} = require("../../controller/category");
const {
  requireSignin,
  validateToken,
  adminMiddleware,
} = require("../../lib/middleware");

var error = require("../error");
const multer = require("multer");
const router = express.Router();
const shortid = require("shortid");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "../../uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage });

router.post(
  "/category",
  validateToken,
  adminMiddleware,
  upload.single("image"),
  addCategory,
  error
);
router.get("/category", getCategories, error);
router.get("/category/:id", getCategory, error);
router.put(
  "/category/:id",
  validateToken,
  adminMiddleware,
  upload.single("image"),
  updateCategory,
  error
);
router.delete(
  "/category/:id",
  validateToken,
  adminMiddleware,
  deleteCategory,
  error
);

module.exports = router;
